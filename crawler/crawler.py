#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re, nltk
from nltk import word_tokenize
import feedparser
import time
import MySQLdb
import collections
import sys
import json
import math
reload(sys)
sys.setdefaultencoding('utf-8')

def getURLS(url):
    slash=0
    urlNew=""
    for letra in url:
        if letra=="/":
            slash=slash+1
        if slash < 3:
            urlNew=urlNew+letra
        else:
             break
    return urlNew

def quitarStopWords(tokensList):

    archivo = open('stopWords.csv', 'r')
    stopwords = archivo.readlines()
    i=0
    #archivo = open('NoticiasSinStopWords.csv', 'a')
    #print tokensList
    for stopword in stopwords:
        x1 = stopword.rstrip('\n')
        i = 0
        while i<len(tokensList):
            #print x1, "==", tokensList[i]
            if x1 == tokensList[i]:
                tokensList.remove(x1)
                i -= 1
                #print tokensList
            i+=1

    return tokensList

def limpiarPalabras(lstWords):
    lstWordsClenaed = []
    listaCaracteres = ["'", "-", "", '¿', '.', '', '', '', "«", "»", "–", "","·","’’","‘‘","’","‘"]
    listaCaracteres.sort()
    #print "limpiando palabras..."
    for word in lstWords:
        auxWord = word
        for caracter in listaCaracteres:
            if caracter in word:
                # print word
                auxWord = auxWord.replace(caracter, '')
                # print auxWord

        if auxWord != "":
            lstWordsClenaed.append(auxWord)
    # print lstWordsClenaed
    lstWordsClenaed.sort()
    strWordsClenaed = ' '.join(lstWordsClenaed)
    return strWordsClenaed

#################DATA CLEANING##########################
def cambiarAcentos(row):
    File = open('correcciones.txt', 'r')
    correcciones = File.readlines()
    File.close()

    for correccion in correcciones:
        rem=correccion.split("||")
        #print rem
        row=row.replace(rem[0], rem[1].rstrip('\n'))
        #print rem[0]," - ",rem[1]

    return row

def quitarEtiquetas(row):
    i = 0
    linea = ""
    escribir = True

    for n in row:
        if (n == '<'):
            escribir = False
        if (n == '>'):
            escribir = True
        if (escribir == True and n != '>'):
            if n != '\n' and n != '|':
                linea = linea + str(n)
            if n == '|':
                i = i + 1
            if n == '|' and i == 2:
                linea = linea + '\n'
                linea = cambiarAcentos(linea)
                print linea
                linea = ""
                i = 0
                return linea

def limpiador(row,nombre):
    linea = ""
    escribir = True
    j=0
    while (j < len(row)-1):

        n = row[j]
        # for n in row:
        if (n == '<'):
            escribir = False
        if (n == '>'):
            escribir = True
            linea = linea + " "
        if (escribir == True and n != '>' and n != '\n'):
            if n != '\n' and n != '\t' and n != '|':
                linea = linea + str(n)

            if row[j] == '|' and row[j + 1] == '|':
                # linea = linea + '\n'
                # print linea
                linea = cambiarAcentos(linea)
                linea=linea+ ',"' + nombre + '"'
                insertarNoticia(linea)

                saveFile = open('TodasLasNoticias.csv', 'a')

                #print linea
                saveFile.write(linea)
                saveFile.write('\n')
                saveFile.close()
                linea = ""

        j = j + 1

def quitarComillas(row):
    linea = ""
    j = 0
    while (j < len(row)):
        n = row[j]
        if n != '"':
            linea = linea + str(n)
        j = j + 1
    return linea

def wordCount(texto):
    tokens = word_tokenize(texto.lower())
    sinStopWordsTokens = quitarStopWords(tokens)
    #strTokens = limpiarPalabras(sinStopWordsTokens)
    strWordsCounted = contarPalabras(sinStopWordsTokens)

    return strWordsCounted

########################################################
def contarPalabras(listaSinStopWordsTocount):
    cuenta1 = collections.Counter(listaSinStopWordsTocount)
    # se pasa a tuplas
    datos = cuenta1.items()


    # for para pasar de tuplas a lista
    frecuenciaDocumento = dict()
    listCounted=[]
    diccionario="{"
    for tupla in datos:
        # print tupla[0],tupla[1]
        listCounted.append(str(tupla[0]) + " " + str(tupla[1]))
        #se crea un diccionario
        #print "++++",tupla[0]
        #diccionario+="'"+tupla[0]+"': "+str(tupla[1])+","
        #frecuenciaDocumento[tupla[0]] = tupla[1]



    return  listCounted
#frecuenciaDoc1,frecuenciaDoc2
import ast
#########################################################
########Calcular similitud##############################
def calcularNumerador(vect1,vect2):
    vector1=vect1.split(' ')
    vector2=vect2.split(' ')
    l1=vector1[0::2]
    l2=vector2[0::2]
    #print l1
    #print l2

    l3=set(l1)&set(l2)
    #print l3

    numerador=0
    for caracteristica in list(l3):
        i1 = vector1.index(caracteristica)
        i2 = vector2.index(caracteristica)
        numerador=numerador+int(vector1[i1+1])* int(vector2[i2 + 1])

    return numerador

def similitudCosine(numerador,sumA,sumB):

    norma = sumA*sumB
    if norma != 0:
        similarity = (numerador/ norma)
    else:
        similarity = 0
    return similarity


def getSimilarNews(consulta):

    texto = quitarComillas(consulta.decode('utf-8'))
    tokens = word_tokenize(texto.lower())
    sinStopWordsTokens=quitarStopWords(tokens)

    strWordsCounted=contarPalabras(sinStopWordsTokens)
    strWordsCountedConsulta = ' '.join(strWordsCounted)


    RaizSumatoriaCuadradosConsulta=0
    for llaveNumero in strWordsCounted:
        numero=llaveNumero.split(' ')
        RaizSumatoriaCuadradosConsulta=RaizSumatoriaCuadradosConsulta+(int(numero[1])*int(numero[1]))


    bd = MySQLdb.connect("127.0.0.1", "root", "1234", "visualizadordenoticias", use_unicode=True, charset="utf8")
    cursor = bd.cursor()
    sql = "SELECT idnew_table,Link,WordCountTexto,RaizSumatoriaCuadrados FROM  newsall"

    cursor.execute(sql)
    dataBase  = cursor.fetchall()
    print len(dataBase),"Recuperando documentos...."
    similarityList=[]
    for row in dataBase:
        numerador = calcularNumerador(row[2], strWordsCountedConsulta)
        similarity = similitudCosine(numerador, row[3], RaizSumatoriaCuadradosConsulta)
        #print type(similarity)
        if similarity !=0:
            #similarityList.append(str(row[0])+","+str(similarity))
            similarityList.append((row[0],similarity,row[1]))

    noticiasRecuperadas = sorted(similarityList, key=lambda tup: tup[1],reverse=True)

    #similarityList.sort(reverse=True)
    #print similarityList
    for noticiaRecuperada in noticiasRecuperadas:
        print noticiaRecuperada[2]


########################################################
########################################################



def raizSumatoriaCuadrados(strWordsCounted):

    SumatoriaCuadrados = 0
    for llaveNumero in strWordsCounted:
        numero = llaveNumero.split(' ')
        SumatoriaCuadrados = SumatoriaCuadrados + (int(numero[1]) * int(numero[1]))
    return math.sqrt(SumatoriaCuadrados)

def insertarNoticia(row):

    #start_time = time()

    datos=row.split('","')

    bd = MySQLdb.connect("127.0.0.1", "root", "1234", "visualizadordenoticias", use_unicode=True, charset="utf8")

    cursor = bd.cursor()

    subDatos=datos[0].split(',"')



    #####Para el texto###########################
    ##Quitar stop words##########################
    texto = quitarComillas(datos[2].decode('utf-8'))
    tokens = word_tokenize(texto.lower())
    sinStopWordsTokens=quitarStopWords(tokens)
    strTokens=limpiarPalabras(sinStopWordsTokens)#regresa un string de las palabras sin stopwords separado por espacios en blanco
    ##Crear word Counted##########################
    strWordsCounted=contarPalabras(sinStopWordsTokens)
    strWordsCountedTexto=' '.join(strWordsCounted)
    RaizSumatoriaCuadradosTexto=0
    for llaveNumero in strWordsCounted:
        numero=llaveNumero.split(' ')
        RaizSumatoriaCuadradosTexto=RaizSumatoriaCuadradosTexto+(int(numero[1])*int(numero[1]))
    ##############################################

    #####Para el Titulo###########################
    ##Quitar stop words##########################
    titulo = quitarComillas(subDatos[1].decode('utf-8'))
    tokens1 = word_tokenize(titulo.lower())
    sinStopWordsTokens1 = quitarStopWords(tokens1)
    strTokens1 = limpiarPalabras(sinStopWordsTokens1)  # regresa un string de las palabras sin stopwords separado por espacios en blanco
    ##Crear word Counted##########################
    strWordsCounted1 = contarPalabras(sinStopWordsTokens1)
    strWordsCountedTitulo = ' '.join(strWordsCounted1)
    RaizSumatoriaCuadradosTitulo = 0

    for llaveNumero in strWordsCounted1:
        numero = llaveNumero.split(' ')
        RaizSumatoriaCuadradosTitulo = RaizSumatoriaCuadradosTitulo + (int(numero[1]) * int(numero[1]))

    strTokensTotal=strTokens1+" "+strTokens
    ##############################################

    fecha=subDatos[0].split('/')
    fecha1=fecha[2] + '/' + fecha[0] + '/' + fecha[1]
    Fuente = getURLS(datos[1])
    if RaizSumatoriaCuadradosTexto != 0:
        try:

            sql = "INSERT INTO newsall(idnew_table,Fuente,Fecha,Titulo,Link,Descripcion,Categoria,TextoSinStopWords,WordCountTexto,RaizSumatoriaCuadrados,WordCountTitulo,RaizSumatoriaCuadradosTitulo) VALUES "
            sql += "(NULL,'" + Fuente + "','" + fecha1+ "',"+'"'+ titulo.decode('utf-8') + '","' + datos[1] + '","' + texto.decode('utf-8') + '","' + datos[3] + ',"' +strTokensTotal.decode('utf-8') + '","' +(strWordsCountedTexto)+ '","' +str(math.sqrt(RaizSumatoriaCuadradosTexto))+ '","' +(strWordsCountedTitulo)+ '","' +str(math.sqrt(RaizSumatoriaCuadradosTitulo))+'")'
        except BaseException, e:
            sql = "INSERT INTO newsall(idnew_table,Fuente,Fecha,Titulo,Link,Descripcion,Categoria,TextoSinStopWords,WordCountTexto,RaizSumatoriaCuadrados,WordCountTitulo,RaizSumatoriaCuadradosTitulo) VALUES "
            sql += "(NULL,'" + Fuente + "','" + fecha1 + "'," + '"' + titulo.decode('utf-8') + '","' + datos[
                1] + '","' + texto + '","' + datos[3] + ',"' +strTokensTotal.decode('utf-8') + '","' +(strWordsCountedTexto)+ '","' +str(math.sqrt(RaizSumatoriaCuadradosTexto))+ '","' +(strWordsCountedTitulo)+ '","' +str(math.sqrt(RaizSumatoriaCuadradosTitulo))+'")'
        print sql

        # Ejecutamos el comando
        try:
            cursor.execute(sql)
            bd.commit()
            # print "ok"
        except BaseException, e:
            saveFile = open('sqlFailed.csv', 'a')
            saveFile.write(sql.encode('utf8'))
            saveFile.write('\n')
            saveFile.close()
            print 'failed cursor execute ', str(e)

def getNews(fecha,link,nombre):

    feed = feedparser.parse(link)
    #print "Feed: ", feed
    #feed = feedparser.parse("http://www.cronicajalisco.com/rss/rssSeccion.php?id=1")
    #time.sleep(5)
    for item in feed["items"]:
        try:
            #description=quitarEtiquetas(str(item["description"]).encode('utf8'))

            linea=fecha+',"'+item[ "title" ]+'","'+item["link"]+'","'+item["description"]+'"||'

            safe_str = linea.encode('utf8')
            row = str(safe_str)
            #saveThis = quitarEtiquetas(saveThis)



            #saveFile = open(nombre+'.csv', 'a')
            #saveFile.write(saveThis)
            #saveFile.write('\n')
            #saveFile.close()
            limpiador(row, nombre)


        except BaseException, e:
            print 'failed query ', str(e)


def updateTables():
    bd = MySQLdb.connect("127.0.0.1", "root", "1234", "visualizadordenoticias", use_unicode=True, charset="utf8")

    cursor = bd.cursor()

    sql="SELECT * FROM newsall ORDER BY Fecha"
    cursor.execute(sql)

    TABLE=cursor.fetchall()

    for row in TABLE:
        Fuente=getURLS(row[3])

        WordCountTexto=wordCount(row[5])
        WordCountTitulo=wordCount(row[3])
        RaizSumatoriaCuadradosTitulo=raizSumatoriaCuadrados(WordCountTitulo)
        RaizSumatoriaCuadrados=raizSumatoriaCuadrados(WordCountTexto)

        strWordsCountedTexto = ' '.join(WordCountTexto)
        strWordsCountedTitulo = ' '.join(WordCountTitulo)

        sql = "INSERT INTO news_all(idnew_table,Fuente,Fecha,Titulo,Link,Descripcion,Categoria,TextoSinStopWords,WordCountTexto,RaizSumatoriaCuadrados,WordCountTitulo,RaizSumatoriaCuadradosTitulo) VALUES "
        sql += "(NULL,'" + row[1] + "','" + str(row[2])+ "',"+'"'+ row[3] + '","' + row[4] + '","' + row[5] + '","' + row[6] + '","' + row[7] + '","' + strWordsCountedTexto + '","' + str(RaizSumatoriaCuadrados) + '","' + strWordsCountedTitulo + '","' + str(RaizSumatoriaCuadradosTitulo) + '")'
        print sql
        cursor.execute(sql)
    bd.commit()
    print "It has finished..."

def main ():
    fecha = time.strftime("%x")
    print "Fecha: "+ fecha
    File = open('ligasRSS.txt', 'r')
    links = File.readlines()
    File.close()

    for link in links:
        # print "link----------------------------------------\n"+link
        ligas = link.split('||')
        try:
            print ligas[0], "----", ligas[1].rstrip('\n')
            getNews(fecha, ligas[0], ligas[1].rstrip('\n'))
        except BaseException, e:
            print 'failed list of links ', str(e)

if __name__ == '__main__':

    #consulta="En el marco de la celebración del Día del Abogado, se entregó la Presea Lic.  Alfonso Cravioto Mejorada.  Con la convicción propia de su profesión, la abogacía, el gobernador José Francisco Olvera Ruiz se comprometió a mantener una administración apegada a la ley y a la norma, que busque para el pueblo hidalguense el principio elemental de la justicia; así se pronunció durante la ceremonia en la que entregó la presea Lic.  Alfonso Cravioto Mejorada al abogado Aarón Samperio Juárez.  Acompañado por representantes de los Poderes del Estado, legisladores, miembros de Barras y Colegios de Abogados y funcionarios estatales, el mandatario aseguró que para mantener una sociedad armónica y articular acciones y programas se requiere contar con una paz social fincada en el estado de Derecho, como la base para alcanzar el desarrollo de Hidalgo.  Agregó que el andamiaje jurídico permitirá descansar los principios de justicia que armonicen adecuadamente las relaciones de la sociedad, y que además por vocación, convicción y formación, son acciones de las políticas públicas para encaminar los proyectos y planes de desarrollo.  La mejor vía de mantener vigente el Estado de derecho es cuando la sociedad lo asume por sí, influye en él sin imposiciones, indicó.  De ahí que, señaló, la necesidad de mantener vigente, actualizado y en permanente transformación el Estado de derecho; por lo que exhortó a los abogados a prepararse a fin de participar en la construcción y análisis de las iniciativas que contribuirán al desarrollo de la estructura jurídica de Hidalgo.  En la medida que una sociedad sea consiente de la aplicación del derecho, es más fácil que éste forme parte de las estructuras sociales, apuntó.  En su oportunidad, el presidente de la Asociación de Abogados del estado, Said Escudero Irra, a nombre de los profesionales del derecho, refrendó el compromiso de trabajar en conjunto con la administración estatal en las acciones que contribuyan a fortalecer el marco jurídico de la entidad y participar de manera constante en las acciones gubernamentales.  Reconoció en Francisco Olvera Ruiz a un gobernante con experiencia en el campo del derecho, que entiende las necesidades y aspiraciones de los hidalguenses; y que trabajará para lograr las condiciones necesarias para el desarrollo de la entidad.  El galardonado, Aarón Samperio Juárez, fue reconocido por el titular del Ejecutivo como un destacado abogado, que lo convierte en un ejemplo a seguir por las nuevas generaciones.  Samperio Juárez es originario del municipio de Epazoyucan, miembro de la primera generación de abogados de la Escuela de Derecho de la Universidad Autónoma del Estado de Hidalgo; ha sido asesor jurídico de la Liga de Comunidades Agrarias y Sindicatos Campesinos en el estado; fundador del Banco Agropecuario Sucursal Pachuca; asesor jurídico de los ayuntamientos de San Salvador, El Arenal y San Agustín Tlaxiaca.  Abogado postulante desde 1965 a la fecha"
    #getSimilarNews(consulta.decode('utf8'))

    main()
